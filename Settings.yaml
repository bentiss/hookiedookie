debug: false
listen_address: "::"
port: 8080

# this is a yaml anchors defintion of a rule, that can be reused multiple
# times in the webhooks.
# See below for an explanation of each key
.default_rule: &default_rule
  not:
    equals:
      key: "key"
      value: "value"

# webhooks list all the registered webhook on this instance
# Note: currently we can have only one webhook per project,
# but we should actually match the pair project/token so we
# can have more than one workload per project
webhooks:
  - # project is the project identifier in gitlab
    # we currently don't support nested groups
    project:
      namespace: "bentiss"
      project: "linux"

    # the registered token for this project.
    # This token must be supplied by the GitLab webhook as part of POST.
    # The rules below are only evaluated if our known token matches the
    # POST token data.
    token: "CHANGE-ME"

    # each project can have one or more workloads,
    # triggered when a set of conditions (rules)
    # are met.
    workloads:
      - # each workload must be given a name
        #
        # It is recommended to have a unique name amongst all
        # the workloads, because that name is used in the logs
        # output
        name: test payload

        # each workload can have a description
        #
        # This description is just informational, and hookiedookie
        # doesn't do anything about it, yet.
        description: |
          A test webhook

          Which doesn't do much

        # use the custom language to match conditions. The
        # stricter we are here, the less time the workload
        # will be spawned
        #
        # A rule is exactly one Operation that must evaluate to
        # true - if true then the target for this workload (see
        # run: below) will be executed.
        #
        # An operation is one of "true", "false",
        # "and", "or", "not", "substring", "follow", "any" or "all".
        # Some operations support nesting, allowing for a single
        # rule to effectively have multiple conditions.
        # As shown in this example, if the top-level operation is
        # "and", multiple operations get evaluated
        rule:

          # and: all of the elements in the array must evaluate
          # to true
          and:

            # using yaml anchors is possible
            - *default_rule

            # equals: the key must be present in the webhook
            # payload and must exactly match the value
            # This example evaluates to true for the yaml snippet:
            # {"object_kind": "push"}
            #
            # When operating on a non-object item directly,
            # the key is optional.
            - equals:
                key: "object_kind"
                value: "push"

            # not: the item must evaluate to false
            - not:

                # follow: go down one nested dict in the webhook
                # payload and execute the `op` condition on that
                # subtree. For example, with the given payload:
                # ```
                # {
                #   "object_kind": "push",
                #   "project": {
                #     "default_branch": "main"
                #   }
                # }
                # ```
                #
                # The "new" payload for any operation below is just
                # `{"default_branch": "main"}`
                follow:
                  key: "project"
                  op:

                    # substring: the key must be present in the webhook
                    # payload and must contain the substring needle
                    substring:
                      key: "default_branch"
                      needle: "wip"

            # any: any item in the list given by the key
            # must evaluate to true. This example is true for this
            # yaml snippet:
            # `branches: [ { name: main }, { name: devel }, { name: test } ]`
            - any:
                key: "branches"
                op:
                  equals:
                    key: "name"
                    value: "main"

            # any: see above, this example shows the use of substring
            # for a list of string items. The substring:key can be omitted
            # in this case. This example is true for this yaml snippet since
            # there is at least one element with the given substring:
            # `branches: [ "0.9", "1.0", "1.1" ]`
            - any:
                key: "tags"
                op:
                  substring:
                    needle: "1."

            # all: all items in the list given by the key
            # must evaluate to true. This example is true for this
            # yaml snippet:
            # `branches: [ { name: dev }, { name: devel }, { name: develop } ]`
            - all:
                key: "branches"
                op:
                  substring:
                    key: "name"
                    needle: "dev"

            # all: see above, this example shows the use of substring
            # for a list of string items. The substring:key can be omitted
            # in this case. This example is false for this yaml snippet since
            # the 0.9 entry does not contain the 1. substring:
            # `branches: [ "0.9", "1.0", "1.1" ]`
            - all:
                key: "tags"
                op:
                  substring:
                    needle: "1."

        # the target to run
        run: ["push_event"]

        # environment variables to forward from hookiedookie to the
        # target. If a value is provided, that value is forwarded instead
        # of the value in hookiedookie's environment.
        env:
          - "GITLAB_TOKEN"
          - "FOO=bar"
