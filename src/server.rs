// SPDX-License-Identifier: MIT License
use crate::settings::{Project, Settings, Webhook, Workload};
use notify::Watcher;
use serde::Serialize;
use std::collections::HashMap;
use std::convert::From;
use std::path::Path;
use std::process::Command;
use std::sync::mpsc::channel;
use std::sync::Mutex;
use tempfile;

use actix_web::{
    dev::Service as _,
    guard::{self, Guard, GuardContext},
    web::{self, Data, ReqData},
    App, HttpMessage, HttpRequest, HttpResponse, HttpServer,
};

#[derive(Debug, Clone, Serialize)]
struct WorkloadError {
    pub message: String,
}

/// Error used in run_workload to make the flow simpler. This error
/// just contains a message since all we can do with it is print it anyway.
impl WorkloadError {
    fn from(msg: String) -> WorkloadError {
        WorkloadError {
            message: String::from(msg),
        }
    }

    fn from_io_error(err: std::io::Error) -> Self {
        WorkloadError::from(format!("IO Error: {err}"))
    }
}

impl std::fmt::Display for WorkloadError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "WorkloadError: {}", self.message)
    }
}

impl From<std::io::Error> for WorkloadError {
    fn from(err: std::io::Error) -> Self {
        WorkloadError::from_io_error(err)
    }
}

impl From<std::string::FromUtf8Error> for WorkloadError {
    fn from(err: std::string::FromUtf8Error) -> Self {
        WorkloadError::from(format!("UTF8 conversion failed: {err}"))
    }
}

fn run_workload(
    workload: Workload,
    payload: serde_json::Value,
) -> Result<WorkloadResult, WorkloadError> {
    // basic check that the command is actually provided
    if workload.run.is_empty() {
        return Err(WorkloadError::from(String::from(
            "invalid workload without run command",
        )));
    }

    let mut cmd = Command::new(workload.run[0].clone());

    cmd.args(workload.run.iter().skip(1));

    cmd.env_clear();
    cmd.env("PATH", std::env::var("PATH").unwrap());

    // Create a private tempdir to run commands in and pass that as TMPDIR.
    let tmp_prefix = "hookiedookie.";
    let tmp_dir = tempfile::Builder::new().prefix(&tmp_prefix).tempdir()?;
    let dirname = tmp_dir.path().to_owned();

    cmd.current_dir(dirname);

    if let Some(envvars) = &workload.env {
        // First iterate over all of our input environment, and
        // pick up the ones we have in scope
        let filtered_env: HashMap<String, String> = std::env::vars()
            .filter(|&(ref k, _)| envvars.contains(k))
            .collect();

        cmd.envs(&filtered_env);

        for var in envvars {
            let parts: Vec<&str> = var.split("=").collect();
            if parts.len() > 1 {
                let key = parts[0];
                let value = parts[1..].join("=");
                cmd.env(key, value);
            }
        }
    }

    let result = WorkloadResult::from(
        &workload,
        cmd.env("HOOKIE_PAYLOAD", payload.to_string())
            .output()
            .map_err(|e| WorkloadError::from_io_error(e)),
    );

    println!("{}", serde_json::json!(result).to_string());
    Ok(result)
}

#[derive(Debug, Clone, Serialize)]
struct WorkloadResult {
    pub name: String,
    pub error: Option<WorkloadError>,
    pub stderr: Option<String>,
    pub stdout: Option<String>,
}

impl WorkloadResult {
    fn new(workload: &Workload) -> Self {
        WorkloadResult {
            name: workload.name.clone(),
            error: None,
            stderr: None,
            stdout: None,
        }
    }

    fn from(workload: &Workload, result: Result<std::process::Output, WorkloadError>) -> Self {
        let mut wl_result = WorkloadResult::new(workload);

        match result {
            Ok(res) => {
                wl_result.stderr = Some(format!("{}", String::from_utf8_lossy(&res.stderr)));
                wl_result.stdout = Some(format!("{}", String::from_utf8_lossy(&res.stdout)));
            }
            Err(err) => wl_result.error = Some(err),
        }

        wl_result
    }
}

// Helper function to be able to test with a proper Webhook struct
async fn webhook_fn(_req: HttpRequest, config: Webhook, body: serde_json::Value) -> HttpResponse {
    // look for all matches
    let workloads: Vec<Workload> = config
        .workloads
        .iter()
        .filter(|&w| w.rule.matches(&body))
        .map(|w| w.clone())
        .collect();

    // run all commands, each in a web::block
    let futures = workloads.iter().map(|workload| {
        let wl_copy = workload.clone();
        let wl_copy2 = workload.clone();
        let body_copy = body.clone();

        web::block(move || {
            run_workload(wl_copy, body_copy)
                .or_else(|e: WorkloadError| {
                    Ok::<WorkloadResult, WorkloadError>(WorkloadResult::from(&wl_copy2, Err(e)))
                })
                .unwrap()
        })
    });

    let mut results: Vec<WorkloadResult> = Vec::new();

    // gather all results
    for result in futures::future::join_all(futures).await {
        match result {
            // If there was a blocking error, something wrong happened,
            // bail out.
            Err(e) => {
                return HttpResponse::InternalServerError()
                    .body(format!("Failed to run workload: {e}"))
            }
            Ok(r) => results.push(r),
        }
    }

    HttpResponse::Ok().body(serde_json::json!(results).to_string())
}

async fn webhook_fn_handler(
    req: HttpRequest,
    data: ReqData<Webhook>,
    body: web::Json<serde_json::Value>,
) -> HttpResponse {
    webhook_fn(req, data.into_inner(), body.into_inner()).await
}

struct ProjectWebhookFound;

impl Guard for ProjectWebhookFound {
    fn check(&self, req: &GuardContext) -> bool {
        // if there is no configuration for this webhook, abort
        req.req_data().get::<Webhook>().is_some()
    }
}

fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        // bind to the url /webhook/namespace/project
        web::resource("/webhook/{namespace}/{project}")
            // ensure the content type is JSON
            .guard(guard::Header("content-type", "application/json"))
            // add request data extracted from global settings
            .wrap_fn(|req, srv| {
                // we are before the request processing
                // ensure we have a gitlab token
                if let (Some(header_value), Some(settings)) = (
                    req.head().headers().get("x-gitlab-token"),
                    req.app_data::<Data<Mutex<Settings>>>(),
                ) {
                    let token = header_value.to_str().unwrap();
                    let match_info = req.match_info();

                    if let Some(webhook) = settings.lock().unwrap().get(
                        // unwrap is fine because if it is not, then
                        // there is a bug in the Actix resource path handler
                        // and we better abort
                        Project::from_name(
                            match_info.get("namespace").unwrap(),
                            match_info.get("project").unwrap(),
                        ),
                        String::from(token),
                    ) {
                        req.extensions_mut().insert(webhook);
                    }
                }

                // future function for processing the response
                srv.call(req)
            })
            // now handle the post
            .route(
                web::post()
                    // ensure the token is valid and we found a webhook
                    // this needs to be here so we are sure the wrap_fn
                    // above is called before
                    .guard(ProjectWebhookFound)
                    // call the webhook function
                    .to(webhook_fn_handler),
            ),
    );
}

fn watch(settings_file: String, app_data: Data<Mutex<Settings>>) {
    // Create a channel to receive the events.
    let (tx, rx) = channel();

    // Automatically select the best implementation for your platform.
    // You can also access each implementation directly e.g. INotifyWatcher.
    let mut watcher: notify::RecommendedWatcher = notify::Watcher::new(
        tx,
        notify::Config::default().with_poll_interval(std::time::Duration::from_secs(10)),
    )
    .unwrap();

    // Add a path to be watched. All files and directories at that path and
    // below will be monitored for changes.
    watcher
        .watch(
            Path::new(settings_file.as_str()),
            notify::RecursiveMode::NonRecursive,
        )
        .unwrap();

    // This is a simple loop, but we are in our own thread, so meh.
    loop {
        match rx.recv() {
            Ok(Ok(notify::Event {
                kind: notify::event::EventKind::Modify(_),
                ..
            })) => {
                println!(
                    r#"{{"system": "{settings_file} written; refreshing configuration ..."}}"#
                );

                match Settings::from_file(settings_file.as_str()) {
                    Ok(new_settings) => {
                        app_data.lock().unwrap().copy_from(&new_settings);
                        println!(r#"{{"system": "{settings_file} successfully changed."}}"#);
                    }
                    Err(error) => eprintln!(
                        r#"{{"system": "error while reading {settings_file}: '{error}'"}}"#
                    ),
                }
            }

            Err(e) => eprintln!(r#"{{"system": "watch error: {e:?}"}}"#),

            _ => {
                // Ignore event
            }
        }
    }
}

pub async fn run(settings_file: String) -> std::io::Result<()> {
    // unwrap may abort, but we are in the init phase
    let settings = Settings::from_file(settings_file.as_str()).unwrap();
    let listen_address = settings.listen_address.to_owned();
    let port = settings.port;
    let app_data = web::Data::new(Mutex::new(settings));

    let app_data_copy = Data::clone(&app_data);

    // We start the thread in detached mode.
    // It will be killed when the program terminates
    std::thread::spawn(move || {
        watch(settings_file, app_data_copy);
    });

    // Start the Actix web server
    HttpServer::new(move || {
        App::new()
            // append global settings to the web server
            // unwrap may abort the server, but we are in the init phase
            .app_data(Data::clone(&app_data))
            .configure(config)
    })
    .bind((listen_address, port))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::rule::Operation;
    use crate::settings::{Project, Workload};
    use actix_service::Service;
    use actix_web::body::to_bytes;
    use actix_web::http::header::{HeaderName, HeaderValue, InvalidHeaderValue, TryIntoHeaderPair};
    use actix_web::web::Bytes;
    use actix_web::{http, test, web::Json};
    use serde::Serialize;
    use std::io::prelude::*;
    use std::thread::sleep;

    #[derive(Debug, Serialize, Clone)]
    #[allow(unused)]
    struct ExamplePayload {
        pub object_kind: String,
    }

    macro_rules! webhook_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[actix_web::test]
                async fn $name() {
                    let (event, workload, body) = $value;
                    let payload = ExamplePayload {
                        object_kind: String::from(event),
                    };
                    let http_req = test::TestRequest::default()
                        .set_json(payload.clone())
                        .to_http_request();
                    let svc_payload = test::TestRequest::default()
                        .set_json(payload)
                        .to_srv_request()
                        .extract::<Json<serde_json::Value>>()
                        .await
                        .unwrap()
                        .into_inner();
                    let req_data = Webhook {
                        project: Project::from_name("testns", "test_project"),
                        token: String::from("test"),
                        workloads: [workload].to_vec(),
                    };
                    let resp = webhook_fn(http_req, req_data, svc_payload).await;
                    assert_eq!(resp.status(), http::StatusCode::OK);
                    let resp_body = to_bytes(resp.into_body()).await.unwrap();
                    assert_eq!(body, resp_body);
                }
            )*
        }
    }

    webhook_tests! {
        push_ok: (
            "push",
            Workload {
                rule: Operation::Equals {
                    key: Some(String::from("object_kind")),
                    value: String::from("push"),
                },
                run: [String::from("true")].to_vec(), // runs /bin/true
                env: None,
                name: String::from("push_rule"),
                description: None,
            }, r#"[{"error":null,"name":"push_rule","stderr":"","stdout":""}]"#
        ),
        push_nok: (
            "puch",
            Workload {
                rule: Operation::Equals {
                    key: Some(String::from("object_kind")),
                    value: String::from("push"),
                },
                run: [String::from("test")].to_vec(),
                env: None,
                name: String::from("push_rule"),
                description: None,
            }, "[]"
        ),
    }

    #[test]
    async fn run_workload_ls_ok() {
        let output = run_workload(
            Workload::new("ls_call", Box::new(["ls"])),
            serde_json::Value::Null,
        )
        .expect("failed to wait on child");

        if let Some(err) = output.error {
            panic!("unexpected error: {:?}", err)
        }

        assert_eq!(
            output.stdout.unwrap(),
            "" // we're in a private tmpdir so no files exist
        );
    }

    #[test]
    async fn run_workload_echo_foo_ok() {
        let output = run_workload(
            Workload::new("echo foo", Box::new(["echo", "foo"])),
            serde_json::Value::Null,
        )
        .expect("failed to wait on child");

        if let Some(err) = output.error {
            panic!("unexpected error: {:?}", err)
        }

        assert_eq!(output.stdout.unwrap(), "foo\n");
    }

    #[test]
    async fn run_workload_ls_missing_nok() {
        let output = run_workload(
            Workload::new("executable is not found", Box::new(["does-not-exist"])),
            serde_json::Value::Null,
        )
        .expect("failed to wait on child");

        assert!(output
            .error
            .unwrap()
            .message
            .contains("No such file or directory"));
    }

    #[test]
    async fn run_workload_ls_file_nok() {
        let output = run_workload(
            Workload::new("ls invalid file", Box::new(["ls", "does-not-exist"])),
            serde_json::Value::Null,
        )
        .expect("failed to wait on child");

        if let Some(err) = output.error {
            panic!("unexpected error: {:?}", err)
        }

        assert_eq!(output.stdout.unwrap(), "");
    }

    #[test]
    async fn run_workload_empty_nok() {
        match run_workload(
            Workload::new("empty command", Box::new([])),
            serde_json::Value::Null,
        ) {
            Err(e) => assert_eq!(
                e.to_string(),
                "WorkloadError: invalid workload without run command"
            ),
            ok => panic!("Unexpected valid result: {ok:?}"),
        }
    }

    struct GitlabTokenHeader {
        pub token: String,
    }

    impl GitlabTokenHeader {
        pub fn from_static(token: &'static str) -> GitlabTokenHeader {
            GitlabTokenHeader {
                token: String::from(token),
            }
        }
        pub fn default() -> GitlabTokenHeader {
            GitlabTokenHeader::from_static("CHANGE-ME")
        }
    }

    impl TryIntoHeaderPair for GitlabTokenHeader {
        type Error = InvalidHeaderValue;

        fn try_into_pair(self) -> Result<(HeaderName, HeaderValue), Self::Error> {
            Ok((
                HeaderName::from_static("x-gitlab-token"),
                HeaderValue::from_str(self.token.as_str()).unwrap(),
            ))
        }
    }

    fn default_settings() -> Settings {
        Settings::from_static(
            r#"
        debug: false
        listen_address: "::"
        port: 8080
        webhooks:
          - project:
              namespace: "testns"
              project: "project1"
            token: "CHANGE-ME"
            workloads: []
        "#,
        )
        .unwrap()
    }

    fn default_settings_with_workloads(workloads: &str) -> Settings {
        // below snippet is indented so the workloads go in the right place
        // when they're defined below in the test methods at the function-level.
        let yaml = format!(
            r#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - project:
          namespace: "testns"
          project: "project1"
        token: "CHANGE-ME"
    {}
"#,
            workloads
        );
        Settings::from_string(yaml).unwrap()
    }

    fn default_uri() -> &'static str {
        "/webhook/testns/project1"
    }

    #[allow(dead_code)]
    fn default_issue_payload() -> &'static str {
        include_str!("../tests/data/issue-event.json")
    }

    #[allow(dead_code)]
    fn default_push_payload() -> &'static str {
        include_str!("../tests/data/push-event.json")
    }

    #[allow(dead_code)]
    fn default_comment_payload() -> &'static str {
        include_str!("../tests/data/comment-event.json")
    }

    macro_rules! request_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[actix_web::test]
                async fn $name() {
                    let (uri, token, json, expected) = $value;
                    let settings = default_settings();
                    let app_data = web::Data::new(Mutex::new(settings));
                    let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;
                    let req = test::TestRequest::post()
                        .uri(uri)
                        .insert_header(token)
                        .insert_header(http::header::ContentType::json())
                        .set_payload(json)
                        .to_request();
                    let res = app.call(req).await.unwrap();

                    assert_eq!(res.status(), expected);
                }
            )*
        }
    }

    request_tests! {
        index_page_returns_404: (
            "/",
            GitlabTokenHeader::default(),
            default_push_payload(),
            http::StatusCode::NOT_FOUND
        ),
        wrong_url_returns_404: (
            "/does/not/exist",
            GitlabTokenHeader::default(),
            default_push_payload(),
            http::StatusCode::NOT_FOUND
        ),
        // correct URL but the project doesn't exist
        wrong_project_returns_405: (
            "/webhook/testns/notfound",
            GitlabTokenHeader::default(),
            default_push_payload(),
            http::StatusCode::METHOD_NOT_ALLOWED
        ),
        // correct URL but the token is invalid
        wrong_token_returns_405: (
            "/webhook/testns/notfound",
            GitlabTokenHeader::from_static("wrong token value"),
            default_push_payload(),
            http::StatusCode::METHOD_NOT_ALLOWED
        ),
        // We only need to test a few json errors. The json
        // parser itself hopefully works correctly, so all we need
        // to check for is that *our* handling of invalid json
        // works, but not what the invalid json actually looks like.
        empty_payload_returns_400: (
            default_uri(),
            GitlabTokenHeader::default(),
            "", // invalid json
            http::StatusCode::BAD_REQUEST
        ),
        invalid_json_returns_400: (
            default_uri(),
            GitlabTokenHeader::default(),
            "{", // json parser error
            http::StatusCode::BAD_REQUEST
        ),
        runaway_quote_returns_400: (
            default_uri(),
            GitlabTokenHeader::default(),
            "{ \"foo\": \"bar }", // json parser error
            http::StatusCode::BAD_REQUEST
        ),
        empty_json_returns_200: (
            default_uri(),
            GitlabTokenHeader::default(),
            "{}", // almost empty json -> it should not match anything
            http::StatusCode::OK
        ),
    }

    #[actix_web::test]
    async fn missing_token_returns_405() {
        let settings = default_settings();
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        // skipping the gitlab token header
        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::METHOD_NOT_ALLOWED);
    }

    #[actix_web::test]
    async fn wrong_content_type_returns_404() {
        let settings = default_settings();
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        // Not setting content-type: application/json here
        let req = test::TestRequest::post().uri(default_uri()).to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::NOT_FOUND);
    }

    #[actix_web::test]
    async fn get_returns_405() {
        let settings = default_settings();
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        let req = test::TestRequest::get() // GET request, not POST
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::default())
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::METHOD_NOT_ALLOWED);
    }

    #[actix_web::test]
    async fn invalid_run_returns_200() {
        let rules = r#"
         - rule:
             equals:
               key: "object_kind"
               value: "push"
        "#;
        let workloads = format!(
            r#"
        workloads: {rules}
           run: []
           name: empty
        "#,
        );
        let settings = default_settings_with_workloads(workloads.as_str());
        println!("{settings:?}");
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::default())
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);
        let body = to_bytes(res.into_body()).await.unwrap();

        assert_eq!(
            String::from(body.as_str()),
            "[{\"error\":{\"message\":\"invalid workload without run command\"},\"name\":\"empty\",\"stderr\":null,\"stdout\":null}]"
        );
    }

    #[actix_web::test]
    async fn multiple_hooks_is_ok() {
        let yaml = r#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - project:
          namespace: "testns"
          project: "project1"
        token: "FIRST-TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           run: ["ls"]
           name: ls
      - project:
          namespace: "testns"
          project: "project1"
        token: "SECOND-TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           run: ["ls"]
           name: ls 2
        "#;
        let settings = Settings::from_string(String::from(yaml)).unwrap();
        println!("{settings:?}");
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("FIRST-TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SECOND-TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(default_push_payload())
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);
    }

    trait BodyTest {
        fn as_str(&self) -> &str;
    }

    impl BodyTest for Bytes {
        fn as_str(&self) -> &str {
            std::str::from_utf8(self).unwrap()
        }
    }

    async fn workload_with_args(rules: &str, expect_output: bool) {
        let cwd_env = std::env::current_dir().unwrap();
        let cwd = cwd_env.to_str().unwrap();
        let workloads = format!(
            r#"
        workloads: {rules}
           run: ["{cwd}/tests/echo-helper.sh"]
           name: echo helper
        "#,
        );
        let settings = default_settings_with_workloads(workloads.as_str());
        println!("{settings:?}");
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;

        // The server receives the payload and deserializes/reserializes it. This
        // reorders keys alphabetically and strips all whitespace, so this string
        // here is how it'll look like after this happens.
        let payload = "{\"object_kind\":\"push\",\"xyz\":{\"sub\":\"val\"},\"zar\":\"bar\",\"zoo\":1,\"zzz\":[\"bat\",\"bar\",\"baz\"]}";
        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::default())
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let body = to_bytes(res.into_body()).await.unwrap();

        if !expect_output {
            assert_eq!("[]", body);
        } else {
            assert!(!String::from(body.as_str()).contains("Failed to find executable"));

            let body_output = format!("{:}", body.as_str());

            let escaped_payload = payload.escape_default();

            println!("\nbody: {}\n", &body_output);

            assert!(body_output.contains(
                format!(
                    "echo-helper.sh called with arguments: \\nPayload: {}",
                    escaped_payload
                )
                .as_str()
            ));
        }
    }

    #[actix_web::test]
    async fn multiple_rules_is_ok() {
        let cwd_env = std::env::current_dir().unwrap();
        let cwd = cwd_env.to_str().unwrap();
        let yaml = format!(
            r#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - project:
          namespace: "testns"
          project: "project1"
        token: "SUPER-SECRET_TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           run: ["echo", "foo"]
           name: echo foo
         - run: ["{cwd}/tests/echo-helper.sh"]
           name: echo helper
        "#
        );
        let settings = Settings::from_string(String::from(yaml)).unwrap();
        println!("{settings:?}");
        let app_data = web::Data::new(Mutex::new(settings));
        let app = test::init_service(App::new().app_data(app_data.clone()).configure(config)).await;
        let payload = "{\"object_kind\":\"push\",\"xyz\":{\"sub\":\"val\"},\"zar\":\"bar\",\"zoo\":1,\"zzz\":[\"bat\",\"bar\",\"baz\"]}";

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let body = to_bytes(res.into_body()).await.unwrap();

        assert!(!String::from(body.as_str()).contains("Failed to find executable"));

        let body_output = format!("{:}", body.as_str());

        let escaped_payload = payload.escape_default();

        println!("\nbody: {}\n", &body_output);

        assert!(body_output.contains(r#""name":"echo foo""#));
        assert!(body_output.contains(
            format!(
                "echo-helper.sh called with arguments: \\nPayload: {}",
                escaped_payload
            )
            .as_str()
        ));
    }

    #[actix_web::test]
    async fn echo_workload_test_equals_ok() {
        let rules = r#"
         - rule:
             equals:
               key: "object_kind"
               value: "push"
        "#;
        workload_with_args(rules, true).await
    }

    #[actix_web::test]
    async fn echo_workload_test_equals_nok() {
        // match on the wrong thing
        let rules = r#"
         - rule:
             equals:
               key: "object_kind"
               value: "commit"
        "#;
        workload_with_args(rules, false).await
    }

    #[actix_web::test]
    async fn echo_workload_test_not_equals_ok() {
        let rules = r#"
         - rule:
             not:
               equals:
                 key: "object_kind"
                 value: "commit"
        "#;
        workload_with_args(rules, true).await
    }

    #[actix_web::test]
    async fn echo_workload_test_not_equals_nok() {
        let rules = r#"
         - rule:
             not:
               equals:
                 key: "object_kind"
                 value: "push"
        "#;
        workload_with_args(rules, false).await
    }

    #[actix_web::test]
    async fn echo_workload_test_and_equals_ok() {
        let rules = r#"
         - rule:
             and:
               - equals:
                   key: "object_kind"
                   value: "push"
               - equals:
                   key: "zar"
                   value: "bar"
        "#;
        workload_with_args(rules, true).await
    }

    #[actix_web::test]
    async fn echo_workload_test_and_equals_nok() {
        let rules = r#"
         - rule:
             and:
               - equals:
                   key: "object_kind"
                   value: "push"
               - equals:
                   key: "zar"
                   value: 1
        "#;
        workload_with_args(rules, false).await
    }

    #[actix_web::test]
    async fn echo_workload_test_or_equals_ok() {
        let rules = r#"
         - rule:
             or:
               - equals:
                   key: "object_kind"
                   value: "commit"
               - equals:
                   key: "zar"
                   value: "bar"
        "#;
        workload_with_args(rules, true).await
    }

    #[actix_web::test]
    async fn echo_workload_test_or_equals_nok() {
        let rules = r#"
         - rule:
             or:
               - equals:
                   key: "object_kind"
                   value: "comit"
               - equals:
                   key: "zoo"
                   value: 1
        "#;
        workload_with_args(rules, false).await
    }

    #[actix_web::test]
    async fn echo_workload_test_any_ok() {
        let rules = r#"
         - rule:
             any:
               key: "zzz"
               op:
                 substring:
                   needle: "at"
        "#;
        workload_with_args(rules, true).await
    }

    #[actix_web::test]
    async fn echo_workload_test_any_nok() {
        let rules = r#"
         - rule:
             any:
               key: "zzz"
               op:
                 substring:
                   needle: "aaa"
        "#;
        workload_with_args(rules, false).await
    }

    #[actix_web::test]
    async fn echo_workload_test_all_ok() {
        let rules = r#"
         - rule:
             all:
               key: "zzz"
               op:
                 substring:
                   needle: "a"
        "#;
        workload_with_args(rules, true).await
    }

    #[actix_web::test]
    async fn echo_workload_test_all_nok() {
        let rules = r#"
         - rule:
             all:
               key: "zzz"
               op:
                 substring:
                   needle: "at"
        "#;
        workload_with_args(rules, false).await
    }

    #[actix_web::test]
    async fn echo_workload_test_follow_ok() {
        let rules = r#"
         - rule:
             follow:
               key: "xyz"
               op:
                 equals:
                   key: "sub"
                   value: "val"
        "#;
        workload_with_args(rules, true).await
    }

    #[actix_web::test]
    async fn echo_workload_test_follow_nok() {
        let rules = r#"
         - rule:
             follow:
               key: "xyz"
               op:
                 not:
                   equals:
                     key: "sub"
                     value: "val"
        "#;
        workload_with_args(rules, false).await
    }

    // just being lazy for the difficult type annotations to extract
    macro_rules! test_init_service {
        ($($value:expr), *) => {
            $(
                {
                    let app_data = $value;
                    test::init_service(App::new().app_data(app_data.clone()).configure(config)).await
                }
            )*
        }
    }

    fn autoreload_workload(settings_filename: String) -> Data<Mutex<Settings>> {
        let mut settings_file = std::fs::File::create(&settings_filename).unwrap();
        settings_file
            .write_all(
                br#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - project:
          namespace: "testns"
          project: "project1"
        token: "SUPER-SECRET_TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           name: echo foo
           run: ["echo", "foo"]"#,
            )
            .unwrap();

        let settings = Settings::from_file(settings_filename.as_str()).unwrap();
        println!("{settings:?}");
        let app_data = web::Data::new(Mutex::new(settings));
        let app_data_copy = app_data.clone();
        std::thread::spawn(move || {
            watch(settings_filename, app_data_copy);
        });

        app_data
    }

    #[actix_web::test]
    async fn autoreload_is_ok() {
        let tmpdir = std::env::temp_dir();
        let settings_filename = format!("{:}/Settings.yaml", tmpdir.display());

        let app_data = autoreload_workload(settings_filename.clone());

        let app = test_init_service!(app_data.clone());
        let payload = "{\"object_kind\":\"push\",\"xyz\":{\"sub\":\"val\"},\"zar\":\"bar\",\"zoo\":1,\"zzz\":[\"bat\",\"bar\",\"baz\"]}";

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let body = to_bytes(res.into_body()).await.unwrap();

        assert!(!String::from(body.as_str()).contains("Failed to find executable"));

        let body_output = format!("{:}", body.as_str());

        println!("\nbody: {}\n", &body_output);

        assert!(body_output.contains(r#""name":"echo foo""#));

        let mut settings_file = std::fs::File::create(&settings_filename).unwrap();
        settings_file
            .write_all(
                br#"
    debug: false
    listen_address: "::"
    port: 8080
    webhooks:
      - project:
          namespace: "testns"
          project: "project1"
        token: "CHANGED-TOKEN"
        workloads:
         - rule:
             equals:
               key: "object_kind"
               value: "push"
           name: echo bar
           run: ["echo", "bar"]"#,
            )
            .unwrap();

        let end = std::time::Instant::now() + std::time::Duration::from_secs(30);

        while std::time::Instant::now() < end {
            // not ideal, but we need to give time for the settings to be reloaded
            sleep(std::time::Duration::from_millis(10));

            if app_data.lock().unwrap().webhooks[0].token != String::from("SUPER-SECRET_TOKEN") {
                break;
            }
        }

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::METHOD_NOT_ALLOWED);
    }

    #[actix_web::test]
    async fn delete_settings_is_ok() {
        let tmpdir = std::env::temp_dir();
        let settings_filename = format!("{:}/TransiantSettings.yaml", tmpdir.display());

        let app_data = autoreload_workload(settings_filename.clone());

        let app = test_init_service!(app_data);
        let payload = "{\"object_kind\":\"push\",\"xyz\":{\"sub\":\"val\"},\"zar\":\"bar\",\"zoo\":1,\"zzz\":[\"bat\",\"bar\",\"baz\"]}";

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        assert_eq!(res.status(), http::StatusCode::OK);

        let body = to_bytes(res.into_body()).await.unwrap();

        assert!(!String::from(body.as_str()).contains("Failed to find executable"));

        let body_output = format!("{:}", body.as_str());

        println!("\nbody: {}\n", &body_output);

        assert!(body_output.contains(r#""name":"echo foo""#));

        std::fs::remove_file(&settings_filename).unwrap();

        // not ideal, but we need to give time for the settings to be reloaded
        sleep(std::time::Duration::from_millis(500));

        let req = test::TestRequest::post()
            .uri(default_uri())
            .insert_header(GitlabTokenHeader::from_static("SUPER-SECRET_TOKEN"))
            .insert_header(http::header::ContentType::json())
            .set_payload(payload)
            .to_request();
        let res = app.call(req).await.unwrap();

        // if the settings file is deleted, we keep the last known config
        assert_eq!(res.status(), http::StatusCode::OK);
    }
}
