// SPDX-License-Identifier: MIT License
use crate::rule::Operation;
use config::{Config, ConfigError, Environment, File};
use serde_derive::Deserialize;

#[derive(Debug, Deserialize, Clone)]
pub struct Project {
    pub namespace: String,
    pub project: String,
}

impl Project {
    pub fn from_name(namespace: &str, project: &str) -> Project {
        Project {
            namespace: String::from(namespace),
            project: String::from(project),
        }
    }
}

impl PartialEq for Project {
    fn eq(&self, other: &Self) -> bool {
        self.namespace == other.namespace && self.project == other.project
    }
}

#[derive(Debug, Default, Deserialize, Clone)]
pub struct Workload {
    #[serde(default)]
    pub rule: Operation,
    pub run: Vec<String>,
    #[serde(default)]
    pub env: Option<Vec<String>>,
    pub name: String,
    #[serde(default)]
    pub description: Option<String>,
}

impl Workload {
    #[allow(unused)]
    pub fn new<'a>(name: &'a str, run: Box<[&'a str]>) -> Self {
        Self {
            rule: Operation::True,
            name: String::from(name),
            description: None,
            env: None,
            run: run.iter().map(|x| String::from(*x)).collect(),
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct Webhook {
    pub project: Project,
    pub token: String,
    pub workloads: Vec<Workload>,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub debug: bool,
    pub listen_address: String,
    pub port: u16,
    pub webhooks: Vec<Webhook>,
}

impl Settings {
    fn defaults() -> Result<config::ConfigBuilder<config::builder::DefaultState>, ConfigError> {
        Ok(Config::builder()
            .set_default("debug", "false")?
            .set_default("listen_address", "::")?
            .set_default("port", "8080")?)
    }

    fn finalize(
        builder: config::ConfigBuilder<config::builder::DefaultState>,
    ) -> Result<Self, ConfigError> {
        // Add in settings from the environment (with a prefix of APP)
        // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
        builder
            .add_source(Environment::with_prefix("HOOKIE"))
            // You may also programmatically change settings
            .build()?
            // You can deserialize (and thus freeze) the entire configuration as
            .try_deserialize()
    }

    pub fn copy_from(&mut self, other: &Self) -> () {
        self.debug = other.debug;
        self.listen_address = other.listen_address.clone();
        self.port = other.port;
        self.webhooks = other.webhooks.clone();
    }

    pub fn from_file(filename: &str) -> Result<Self, ConfigError> {
        let builder = Settings::defaults()?.add_source(File::with_name(filename));
        Settings::finalize(builder)
    }

    #[allow(dead_code)]
    pub fn from_static(string: &'static str) -> Result<Self, ConfigError> {
        let builder =
            Settings::defaults()?.add_source(File::from_str(string, config::FileFormat::Yaml));
        Settings::finalize(builder)
    }

    #[allow(dead_code)]
    pub fn from_string(string: String) -> Result<Self, ConfigError> {
        let builder = Settings::defaults()?
            .add_source(File::from_str(string.as_str(), config::FileFormat::Yaml));
        Settings::finalize(builder)
    }

    pub fn get(&self, project: Project, token: String) -> Option<Webhook> {
        for webhook in self.webhooks.iter() {
            if project == webhook.project && token == webhook.token {
                return Some(webhook.clone());
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn load_default_settings() {
        assert!(Settings::from_file("Settings.yaml").is_ok());
    }

    #[test]
    fn load_from_string() {
        let config = r#"
        port: 123
        debug: true
        listen_address: 'abc'
        webhooks: []
        "#;

        let s = Settings::from_static(config).unwrap();
        assert_eq!(s.port, 123);
        assert_eq!(s.debug, true);
        assert_eq!(s.listen_address, "abc");
    }
}
