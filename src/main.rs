// SPDX-License-Identifier: MIT License
mod rule;
mod server;
mod settings;

use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    settings_file: Option<String>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let args = Args::parse();

    let settings = args.settings_file.unwrap_or(String::from("Settings.yaml"));

    server::run(settings).await
}
