#!/usr/bin/env bash
# SPDX-License-Identifier: MIT License
#
# Echos all arguments into the $ECHOHELPER_SOCKET file
#

set -x

echo "$(basename "$0") called with arguments: $*"

if [[ -z "$HOOKIE_PAYLOAD" ]]; then
   echo "HOOKIE_PAYLOAD not set"
   exit 1
fi

echo "Payload: $HOOKIE_PAYLOAD"

if ! command -v socat; then
   echo "Install socat for this to work"
   exit 1
fi

if [[ -z "$ECHOHELPER_SOCKET" ]]; then
   echo "\$ECHOHELPER_SOCKET not set"
   exit 1
fi
if ! [[ -e "$ECHOHELPER_SOCKET" ]];  then
  echo "socket $ECHOHELPER_SOCKET does not exist"
  exit 1
fi

shift # Drop $0, don't need that one

echo -n "$HOOKIE_PAYLOAD" | socat - UNIX-CONNECT:"$ECHOHELPER_SOCKET"
